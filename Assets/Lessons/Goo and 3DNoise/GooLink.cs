﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GooLink : MonoBehaviour
{
    public Transform src;
    public Transform dst;
    public Material linkMat;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = 0.5f * (src.position + dst.position);
        var dir = dst.position - src.position;
        var dist = dir.magnitude / transform.lossyScale.y;
        transform.localScale = new Vector3(dist, 1f, 1f);
        linkMat.SetFloat("_Length", dir.magnitude);
        var angle = Vector2.SignedAngle(Vector3.right, dir);
        transform.eulerAngles = Vector3.forward * angle;
        src.eulerAngles = Vector3.forward * angle;
        dst.eulerAngles = Vector3.forward * (angle + 180f);
    }
}
