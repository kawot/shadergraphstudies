﻿using UnityEngine;
using System.Collections;

public class MoveToMouse : MonoBehaviour
{
    public float speed = 1f;

    Camera cam;

    // Use this for initialization
    void Start()
    {
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        var mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
        mousePos.z = 0f;
        var dir = mousePos - transform.position;
        if (dir.sqrMagnitude > 1f) dir.Normalize();
        transform.position += dir * speed * Time.deltaTime;
    }
}