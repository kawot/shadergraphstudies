﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RelativeSpaceObject : MonoBehaviour
{
    public Transform relativePoint;

    private Vector3 startPos;

    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;   
    }

    // Update is called once per frame
    void Update()
    {
        if (relativePoint == null)
        {
            Debug.LogError("Space Ship is not set!");
            return;
        }

        var dir = startPos - relativePoint.position;
        var dist = dir.magnitude + 1f;
        var logDist = Mathf.Log10(dist) * 0.5f;
        transform.position = relativePoint.position + logDist * dir.normalized;

        var scale = 1f/(dist*dist);
        transform.localScale = Vector3.one * scale;
    }
}
