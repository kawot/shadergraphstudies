﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class StarScaleTween : MonoBehaviour
{
    public float growthTime = 2f;
    public float growthSize = 1.4f;
    [Space]
    public float fallTime = 0.2f;
    [Space]
    public ParticleSystem ps;
    [Space]
    [Header("Shake")]
    public float shakeDelay = 1f;
    public float shakeDuration = 1f;
    public float shakeStrength = 10f;
    public int shakeVibrato = 20;

    Sequence sequence;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Scale()
    {
        ResetToStart();

        if (sequence != null) sequence.Kill();
        sequence = DOTween.Sequence();

        sequence
            .Append(transform.DOScale(growthSize, growthTime).SetEase(Ease.Linear))
            .Join(transform.DOShakePosition(shakeDuration,shakeStrength, shakeVibrato, 90f, false, false).SetDelay(shakeDelay))
            .Append(transform.DOScale(1f, fallTime).SetEase(Ease.OutQuad));

        sequence.Play();
        ps.Play();
    }

    private void ResetToStart()
    {
        transform.localScale = Vector3.one;
    }
}
