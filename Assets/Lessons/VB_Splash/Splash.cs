﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Splash : MonoBehaviour
{
    public Material material;
    public Transform quad;
    public float duration = 1f;
    public float startSize = 1f;
    public float endSize = 4f;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            StartCoroutine(Splashing());
        }
    }

    IEnumerator Splashing()
    {
        var age = 0f;
        while(age < 1f)
        {
            yield return new WaitForEndOfFrame();
            age += Time.deltaTime / duration;
            material.SetFloat("_Age", age*age);
            transform.localScale = Vector3.one * Mathf.Lerp(startSize, endSize, age * age);          
        }
    }
}
