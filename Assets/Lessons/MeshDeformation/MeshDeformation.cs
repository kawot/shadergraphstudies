﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
public class MeshDeformation : MonoBehaviour
{
    [SerializeField] float dragRadius = 1f;
    [SerializeField] float smoothBack = 1f;

    Camera cam;
    Mesh mesh;
    Vector3[] verts;
    Vector3[] origins;
    int[] tris;

    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
        mesh = GetComponent<MeshFilter>().mesh;
        verts = mesh.vertices;
        origins = mesh.vertices;
        tris = mesh.triangles;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            var drag = new Vector3(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"), 0f);
            var mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
            mousePos.z = 0f;

            for (var i = 0; i < verts.Length; i++)
            {
                var vert = transform.TransformPoint(verts[i]);
                vert.z = 0f;
                var dist = Vector3.Distance(vert, mousePos);
                if (dist < dragRadius)
                {
                    //var affect = Mathf.SmoothStep(dragRadius, 0f, dist);
                    var affect = 1f - dist/dragRadius;
                    affect = 3f * affect * affect - 2f * affect * affect * affect;
                    vert += drag * affect;
                    verts[i] = transform.InverseTransformPoint(vert);
                }
                else
                {
                    verts[i] = Vector3.Lerp(verts[i], origins[i], Time.deltaTime * smoothBack);
                }
            }
            mesh.vertices = verts;
        }
    }
}
