﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
public class CircleMesh : MonoBehaviour
{
    
    public float radius;
    public int segments;

    MeshFilter viewMeshFilter;
    Mesh viewMesh;
    float mass;

    void Start()
    {
        viewMeshFilter = GetComponent<MeshFilter>();
        viewMesh = new Mesh();
        viewMesh.name = "View Mesh";
        viewMeshFilter.mesh = viewMesh;

        GenerateMesh(radius, segments);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void GenerateMesh(float r, int s)
    {
        if (s < 3) return;

        Vector3[] vertices = new Vector3[s + 1];
        int[] triangles = new int[3 * s];

        vertices[0] = Vector3.zero;
        var a = 360f / s;
        for (var i = 0; i < s; i++)
        {
            vertices[i + 1] = DirFromAngle(a * i, true) * r;
            if (i == 0)
            {
                triangles[0] = 0;
                triangles[1] = s;
                triangles[2] = 1;
            }
            else
            {
                triangles[3 * i + 0] = 0;
                triangles[3 * i + 1] = i;
                triangles[3 * i + 2] = i + 1;
            }
        }

        viewMesh.Clear();

        viewMesh.vertices = vertices;
        viewMesh.triangles = triangles;
        viewMesh.RecalculateNormals();

        mass = segments * radius;
    }

    public void RestoreNeighbors(float smooth)
    {
        var verts = viewMesh.vertices;
        for (var i = 1; i < verts.Length; i++)
        {
            var left = i + 1;
            var right = i - 1;
            if (left == verts.Length) left = 1;
            if (right == 0) right = verts.Length - 1;
            verts[i] = verts[i].normalized * Mathf.Lerp(verts[i].magnitude, (verts[left].magnitude + verts[right].magnitude)*0.5f, Time.deltaTime * smooth);
        }
        viewMesh.vertices = verts;
    }

    public void RestoreMass()
    {
        var verts = viewMesh.vertices;
        var m = 0f;
        for (int i = 1; i < verts.Length; i++)
        {
            m += verts[i].magnitude;
        }
        var scale = mass / m;
        for (int i = 1; i < verts.Length; i++)
        {
            verts[i] *= scale;
        }
        viewMesh.vertices = verts;
    }

    public Vector3 DirFromAngle(float angleInDegrees, bool angleIsGlobal)
    {
        if (!angleIsGlobal)
        {
            angleInDegrees += transform.eulerAngles.z;
        }
        return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), Mathf.Cos(angleInDegrees * Mathf.Deg2Rad), 0f);
    }

    public bool IsInside(Vector2 p)
    {
        var a = Vector2.SignedAngle(p, Vector2.up);
        if (a < 0f) a += 360f;
        var n = Mathf.CeilToInt(a * segments / 360f);
        var spokeA = viewMesh.vertices[n].magnitude;
        var spokeB = (n == segments) ? viewMesh.vertices[1].magnitude : viewMesh.vertices[n + 1].magnitude;
        var arg = (a % (360f / segments)/(360f/segments));
        var r = Mathf.Lerp(spokeA, spokeB, arg);
        //Debug.Log("N: " + n +"  p: " + p.magnitude + "  r: " + r + "  A: " + spokeA+ "  B: " +spokeB + "  arg: " + arg);
        return p.magnitude < r;
    }

    public float Difference(Vector2 p)
    {
        var a = Vector2.SignedAngle(p, Vector2.up);
        if (a < 0f) a += 360f;
        var n = Mathf.CeilToInt(a * segments / 360f);
        var spokeA = viewMesh.vertices[n].magnitude;
        var spokeB = (n == segments) ? viewMesh.vertices[1].magnitude : viewMesh.vertices[n + 1].magnitude;
        var arg = (a % (360f / segments) / (360f / segments));
        var r = Mathf.Lerp(spokeA, spokeB, arg);
        return p.magnitude - r;
    }

    public void Deform(Vector2 p, float arc, bool adding)
    {
        var d = Difference(p);
        if ((adding && d < 0) || (!adding && d > 0)) return;

        var a = Vector2.SignedAngle(p, Vector2.up);
        var startA = a - 0.5f * arc;
        var endA = a + 0.5f * arc;

        var angle = startA;
        var verts = viewMesh.vertices;
        var level = p.magnitude;
        while (angle <= endA)
        {
            var k = SmoothStep(startA, 0.5f * (startA + endA), angle) * (1f - SmoothStep(0.5f * (startA + endA), endA, angle));
            var posAngle = angle;
            if (posAngle < 0f) posAngle += 360f;
            var n = Mathf.CeilToInt(posAngle * segments / 360f);
            var r = verts[n].magnitude;
            //r = Mathf.Lerp(r, r + d, k);
            if (adding) r = Mathf.Max(r, Mathf.Min(level, Mathf.Lerp(r, r + d, k)));
            else r = Mathf.Min(r, Mathf.Max(level, Mathf.Lerp(r, r + d, k))); 
            verts[n] = verts[n].normalized * r;

            angle += 360f / segments;
        }
        viewMesh.vertices = verts;
    }

    private float SmoothStep(float a, float b, float t)
    {
        if (t < a) return 0f;
        else if (t > b) return 1f;
        else
        {
            var arg = (t - a) / (b - a);
            return 3f * arg * arg - 2f * arg * arg * arg;
        }
    }
}
