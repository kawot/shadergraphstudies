﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleDeformation : MonoBehaviour
{
    [SerializeField] float dragRadius = 1f;
    [SerializeField] float minRadius = 1f;
    [SerializeField] float maxRadius = 10f;
    [SerializeField] float dragArc = 60f;
    [SerializeField] float smoothNeighbors = 1f;
    [SerializeField] float escapeRadius = 1f;
    [SerializeField] float escapeSpeed = 1f;
    [SerializeField] float returnSpeed = 10f;
    [SerializeField] CircleMesh mesh;

    Camera cam;
    bool isInside = false;

    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        var mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
        mousePos.z = 0f;

        if (Input.GetMouseButtonDown(0)) isInside = mesh.IsInside(transform.InverseTransformPoint(mousePos));

        if (Input.GetMouseButton(0))
        {
            var bias = (isInside) ? dragRadius : -dragRadius;
            var pos = transform.InverseTransformPoint(mousePos).normalized * Mathf.Clamp(transform.InverseTransformPoint(mousePos).magnitude + bias, minRadius, maxRadius);
            mesh.Deform(pos, dragArc, isInside);
            mesh.RestoreMass();

            var move = Vector3.zero;
            var escapeVector = transform.position - mousePos;
            if (!isInside)
            {
                if (escapeVector.magnitude < minRadius + escapeRadius)
                {
                    move = escapeVector.normalized * (escapeRadius / (escapeVector.magnitude - minRadius) - 1f) * escapeSpeed;
                    move -= transform.position * returnSpeed;
                }
            }
            else
            {
                if (escapeVector.magnitude > maxRadius)
                {
                    //move = 
                }
            }
            transform.position += move * Time.deltaTime;
        }

        mesh.RestoreNeighbors(smoothNeighbors);
    }
}
