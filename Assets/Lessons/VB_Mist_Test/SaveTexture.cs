﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class SaveTexture : MonoBehaviour
{

    public RenderTexture rTex;
    public Vector2Int Resolution;
    public Material mat;
    public string path = "Assets/Test.png";

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            Debug.Log("BLIT!");
            BakeTexture();
        }
    }


    void BakeTexture()
    {
        //render material to rendertexture
        Graphics.Blit(null, rTex, mat);

        //transfer image from rendertexture to texture
        Texture2D texture = new Texture2D(Resolution.x, Resolution.y);
        RenderTexture.active = rTex;
        texture.ReadPixels(new Rect(Vector2.zero, Resolution), 0, 0);

        //save texture to file
        byte[] png = texture.EncodeToPNG();
        File.WriteAllBytes(path, png);
        //AssetDatabase.Refresh();
    }

}
