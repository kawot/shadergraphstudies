﻿Shader "TestShaders/Emission"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
		_Intensity ("Intensity", float) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
            };



            v2f vert (appdata v)
            {
				v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
				return o;
            }

			fixed4 _Color;
			half _Intensity;

            fixed4 frag (v2f i) : SV_Target
            {    
                fixed4 col = _Color * _Intensity;
                return col;
            }
            ENDCG
        }
    }
}
