﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class StreamTarget : MonoBehaviour
{
    public Transform target;
    public float lifeTime = 1f;
    public float density = 5f;
    public float minEmission = 5f;

    ParticleSystem.VelocityOverLifetimeModule vm;
    ParticleSystem.EmissionModule em;

    // Start is called before the first frame update
    void Start()
    {
        var ps = GetComponent<ParticleSystem>();
        var mm = ps.main;
        mm.startLifetimeMultiplier = lifeTime;
        vm = ps.velocityOverLifetime;
        em = ps.emission;
    }

    // Update is called once per frame
    void Update()
    {
        vm.orbitalOffsetXMultiplier = target.localPosition.x;
        vm.orbitalOffsetYMultiplier = target.localPosition.y;
        var dist = Vector2.Distance(transform.position, target.position);
        vm.radialMultiplier = -dist / lifeTime;
        em.rateOverTimeMultiplier = Mathf.Max(density * dist, minEmission);
    }
}
